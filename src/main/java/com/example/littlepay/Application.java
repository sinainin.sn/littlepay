package com.example.littlepay;

import lombok.AllArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@AllArgsConstructor
public class Application {

    public static void main(String[] args) throws Exception {
        ApplicationDatabaseInitializer.verify();
        SpringApplication.run(Application.class, args);
    }
}
